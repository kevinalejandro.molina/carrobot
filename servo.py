from time import sleep
from thread_task import Sleep
import ev3_dc as ev3

with ev3.Motor(
    ev3.PORT_C,
    protocol=ev3.USB,
    speed=80
) as my_motor:
    movement_plan = (
        my_motor.move_to(360) +
        Sleep(5) +
        my_motor.move_to(0, speed=100, ramp_up=90, ramp_down=90, brake=True) +
        Sleep(0.5) +
        my_motor.stop_as_task(brake=True)
    )

    movement_plan.start()

    movement_plan.join()
