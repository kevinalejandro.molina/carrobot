import ev3_dc as ev3
from time import sleep

my_motor = ev3.Motor(
    ev3.PORT_A,
    protocol=ev3.USB
)
my_motor.verbosity = 1
my_motor.sync_mode = ev3.STD

my_motor.start_move()
sleep(1)
my_motor.start_move(direction=-1)
sleep(1)
my_motor.stop()
