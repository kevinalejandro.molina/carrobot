import ev3_dc as ev3

with ev3.TwoWheelVehicle(
    0.01518,  # radius_wheel
    0.15495,  # tread
    protocol=ev3.USB
) as my_vehicle:
    parcours = (
        my_vehicle.drive_straight(0.5, speed=85) +
        my_vehicle.drive_turn(100, 0.0, speed=85) +
        my_vehicle.drive_straight(0.5, speed=85)
    )
    parcours.start(thread=False)
